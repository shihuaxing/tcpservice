#!/usr/bin/env python3
import socket
import threading
import socketserver
import struct
import json,sys
from nlp import NLPTool
nlpTool = NLPTool()
content = '小野寺五典在出任防卫大臣前，提出日本应具备攻击敌方基地的能力。但自卫队拥有航母将严重冲击日本的专守防卫原则。'
result = nlpTool.allsegment(content, model='standard')

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
        pass

class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.
    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        conn = self.request
        addr = self.client_address
        # 上面两行代码，等于 conn,addr = socket.accept()，只不过在socketserver模块中已经替我们包装好了，还替我们包装了包括bind()、listen()、accept()方法
        while 1:
            head = self.request.recv(4)
            if not head:
                break
            data_length = struct.unpack('>L',head)[0]
            # 循环接收包. 否则容易出错
            had_received = 0
            data = bytes()
            while had_received < data_length:
                part_body = self.request.recv(data_length - had_received)
                data += part_body
                part_body_length = len(part_body)
                had_received += part_body_length
            input_ = json.loads(str(data, encoding='utf-8'))
            content = input_['query']
            result = nlpTool.allsegment(content, model='standard')
            result = bytes(json.dumps(result),'utf-8')
            result_length =  struct.pack('>L',len(result))
            print(result_length)

            package = result_length + result  #result_json = bytes('{1:1}','utf-8')
            self.request.sendall(package)
            print(len(data),len(package))
        conn.close()
if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", int(sys.argv[1])
    #with ThreadedTCPServer((HOST, PORT), MyTCPHandler) as server:
    with socketserver.TCPServer((HOST, PORT), MyTCPHandler) as server:
        # Activate the server; this will keep running until you
        # interrupt the program with Ctrl-C
        server.serve_forever()
