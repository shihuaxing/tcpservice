#/usr/bin/env python3
#*--coding=utf-8--*
import socket
import sys
import time
import logging
import json
import struct
logging.basicConfig(filename='trasnfer.log',
                    format='%(asctime)s -%(name)s-%(levelname)s-%(module)s:%(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S %p',
                    level=logging.ERROR)
class TcpNlp(object):
    '''
     对接基于tcp链接的scoket server的客户端. 也通过复写transfer方法来实现其他格式数据的对接.
     cs双方传递数据为json格式, 服务器端解析json格式数据
     函数实例化时, 需要给定:
        host: socket服务器的ip地址, 默认为本机 127.0.0.0
        port: socket 对应的端口
        data: json/plain 传输数据的格式, json还是纯文本, 默认为json 
     transfer: transfer方法是需要复写的数据处理函数.
     close: close方法是在处理接收后关闭链接的方法.
     '''

    def __init__(self, port, host='127.0.0.0', type='json'):
        self.host = host
        self.port = int(port)
        if type in ['json','plain']:
            self.data_type = type
        else:
            raise TypeError('type must be json or plain')
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
 #       self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1) 
        try:
            self.sock.connect((self.host,self.port))
            self.sock.settimeout(None)
        except OSError as err:
            logging.info('connection existing, back to connect!')
            logging.debug(err)
        except Exception as err:
            logging.error(err)

    def transfer(self, data, debug=False):
        if (not isinstance(data,dict)) and (self.data_type == 'json') :
            if isinstance(data,bytes):
                data = str(data,'utf-8')
            try:
                json.loads(data)
            except Exception as err:
                raise TypeError('json data format error\n',err)
        if isinstance(data,str):
            data = bytes(data,'utf-8')
        elif isinstance(data,dict):
            data = json.dumps(data)
            data = bytes(data,'utf-8')
        elif  not isinstance(data,bytes):
            raise TypeError('data must be string or dict')
        content = data
        content_length = struct.pack('>L',len(content))
        package = content_length + content  #result_json = bytes('{1:1}','utf-8')
        self.sock.sendall(package)
        received_length = struct.unpack('>L',self.sock.recv(4))[0]
        had_received = 0
        received = bytes()
        while had_received < received_length:
            part_body = self.sock.recv(received_length - had_received)
            received += part_body
            part_body_length = len(part_body)
            had_received += part_body_length
        if isinstance(received,bytes):
            received = str(received, "utf-8")
        elif not isinstance(received,str):
            raise TypeError('received data must be bytes or string')
        if debug:
            print("Sent:     {}".format(data))
            print("Received: {}".format(received))
        return received

    def close(self):
        self.sock.close()

